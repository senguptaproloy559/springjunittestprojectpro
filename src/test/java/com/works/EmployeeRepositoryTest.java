package com.works;
import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import com.works.dao.EmployeeRepositoryDao;
import com.works.model.Employee;


@RunWith(SpringRunner.class)
@DataJpaTest
public class EmployeeRepositoryTest {
	
	@Autowired
    private EmployeeRepositoryDao employeeRepositoryDao;
	
	@Autowired
	private TestEntityManager entityManager;
	
	@Test
	public void testSaveEmployee(){
		Employee ticket = getEmployee();
		Employee savedInDb = entityManager.persist(ticket);
		Employee getFromDb = employeeRepositoryDao.findById(savedInDb.getId());
		
		assertThat(getFromDb).isEqualTo(savedInDb);
	}

	
	private Employee getEmployee() {
		Employee ticket = new Employee();
		ticket.setFirstName("Animesh");
		ticket.setLastName("Mandal");
		ticket.setEmail("animesh@gmail.com");
		return ticket;
	}
}
