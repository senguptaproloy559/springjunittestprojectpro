package com.works.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.works.model.Employee;

@Repository
public interface EmployeeRepositoryDao extends CrudRepository <Employee, Long> 
{
	 Employee findById(int id);
}
